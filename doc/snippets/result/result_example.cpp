#include "result/result.hpp"

#include <iostream>

using namespace bob::result;

//! [Using Ok and Err]
Result<int> divide(int numerator, int divisor) {
    if(divisor == 0) { return Err<int>(); }
    return Ok<int>(numerator / divisor);
}
//! [Using Ok and Err]

int main() {
//! [Getting a result]
    using Result = Result<int>;
    auto result = divide(42, 13);
    switch(auto value = result(Result::ErrorHandling)) {
        case Res::Ok:  std::cout << "Successful calculation: " << value() << std::endl; break;
        case Res::Err: std::cout << "Error occured!"; break;
    }
//! [Getting a result]
    
    return 0;
}
