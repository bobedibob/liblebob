/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "assert/backtrace.hpp"

#include "console/richtext.hpp"

#include <iostream>
#include <iomanip>

// includes for backtrace
#include <execinfo.h>
#include <sstream>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <regex>

namespace bob {
namespace assert {


void printBacktrace(const std::string& description) {
    using namespace bob::console;
    std::cerr << RichText {{Color::Red, Style::Bright}};
    std::cerr << std::setw ( 3 ) << 0 << ": Ooops -> " << description;
    std::cerr << RichText {{RichText::Reset}};
    std::cerr << std::endl;
    
    // this is hacky, but it works
    std::smatch matches;
    std::regex regEx {"(\\+)(0x[0-9a-fA-F].*)(\\))"};   // matches words beginning by "+", followed by"0x.." and ends with ")"
    std::vector<std::string> functionOffsets;

    // get backtrace
    void *returnAddresses[20];
    char **symbols = (char **)NULL;
    int size;

    //TODO: error checking for C-functions
    // get void*'s for all entries on the stack
    size = backtrace(returnAddresses, 20);
    symbols = backtrace_symbols(returnAddresses, size);
    for(int i = 0; i < size; i++) {
        auto message = std::string(symbols[i]);
        if(std::regex_search(message, matches, regEx))
        {
            if(matches.size() == 4) {
                functionOffsets.push_back(matches[2]);
            }
        }
    }
    // backtrace_symbols need to be freed; backtrace not
    free(symbols);
    
    constexpr int MaxPath { 1024 };
    char executable[MaxPath] { };

    if(readlink ( "/proc/self/exe", executable, MaxPath-1 ) >= 0) {
        std::cout << RichText {{Color::Yellow}};

        int i = 1;
        for(const auto& offset: functionOffsets) {
            std::cout << std::setw ( 3 ) << i << ": " << std::flush;
            std::string cmdA2L = "addr2line -p -C -f -e " + std::string(executable) + " " + offset;
            if(system(cmdA2L.c_str()) == -1) {
                std::cout << "Could not call addr2line! errno: " << strerror(errno) << std::endl;
            }
            
            ++i;
        }
        std::cout << RichText {{RichText::Reset}};
    }
}

} // namespace assert
} // namespace bob
