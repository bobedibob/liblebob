/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "assert/assert.hpp"

#include "assert/backtrace.hpp"

#include <cassert>
#include <string>

namespace bob {
namespace assert {

void Ensures(const bool condition) {
    AssertHandler::doAssert(condition, "Assert!");
}

void Ensures(const bool condition, const std::string& description) {
    AssertHandler::doAssert(condition, description);
}

void Expects(const bool condition) {
    AssertHandler::doAssert(condition, "Assert!");
}

void Expects(const bool condition, const std::string& description) {
    AssertHandler::doAssert(condition, description);
}

std::function<void(const bool, const std::string&)> AssertHandler::handler = AssertHandler::defaultHandler;

void AssertHandler::doAssert(const bool condition, const std::string& description) {
    AssertHandler::handler(condition, description);
}

void AssertHandler::defaultHandler(const bool condition, const std::string& description) {
    if(!condition) {
        printBacktrace(description);
    }
    assert(condition);
}

void AssertHandler::noRuntimeChecksHandler(const bool condition, const std::string& description) {
    static_cast<void>(condition);
    static_cast<void>(description);
}

} // namespace assert
} // namespace bob
