/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "console/richtext.hpp"

#include "catch.hpp"

#include <sstream>

TEST_CASE("Check rich text console output") {
    using namespace bob::console;
    
    std::stringstream stream;
    
    SECTION("Setting one format") {
        stream << RichText {{Color::Red}};
        auto format = std::string(stream.str());
        constexpr auto expectedFormatString = "\033[31m";
        CHECK(format == expectedFormatString);
    }
    
    SECTION("Setting multiple formats") {
        stream << RichText {{Color::Red, Style::Bright, Style::Crossedout}};
        auto format = std::string(stream.str());
        constexpr auto expectedFormatString = "\033[31;1;9m";
        CHECK(format == expectedFormatString);
    }
    
    SECTION("Resetting the format") {
        stream << RichText {{RichText::Reset}};
        auto format = std::string(stream.str());
        constexpr auto expectedFormatString = "\033[0m";
        CHECK(format == expectedFormatString);
    }
}
