/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */


// we use the fact that most developers don't refer to the global namespace with the leading :: (just think of it like {global namespace}::)
// -> when using std::cout in namespace ::foo::bar, it actually is ::foo::bar::std::cout
// -> if there is no std namespace in ::foo::bar, the compiler looks for ::foo::std::cout and eventually for ::std::cout

// test/mock/std/atomic.hpp also includes <atomic>, therefore the include guards from std::atomic prevents the std::atomic
// to live in namespace bob::test::mock when included from atomic_test_object.hpp in the test
#include "test/mock/std/atomic.hpp"

namespace bob {
namespace test {
namespace mock {

namespace std {
    // we have to redirect some std usage to the global std namespace
    using ::std::cout;
    using ::std::endl;
    
    using ::std::memory_order_acq_rel;
    using ::std::memory_order_relaxed;
} // namespace std

/** this is the header of the class to test **/
#include "atomic_test_object.hpp"

} // namespace mock
} // namespace test
} // namespace bob

#include "assert/backtrace.hpp"

#include "catch.hpp"

#include <utility>

using bob::test::mock::AtomicFoo;

TEST_CASE("AtomicMock") {
    AtomicFoo foo;
    int newValue {37};
    int oldValue = foo.peak();
    CHECK(foo.push(newValue) == oldValue);
    CHECK(foo.peak() == newValue);
    
    constexpr int overrideExchangeValue { 42 };
    bob::test::mock::std::atomic<int>::setOverrideExchangeValue(overrideExchangeValue);
    newValue = 73;
    oldValue = foo.peak();
    CHECK(foo.push(newValue) == oldValue);
    CHECK(foo.peak() == overrideExchangeValue);
    bob::test::mock::std::atomic<int>::unsetOverrideExchangeValue();
}

#include <thread>
class ThreadLocalTest {
private:
    std::atomic_flag m_pushThreadSlotOccupied = ATOMIC_FLAG_INIT;
    std::atomic_flag m_popThreadSlotOccupied = ATOMIC_FLAG_INIT;
    
public:
    void push(std::string id) {
        thread_local bool firstThreadAccess { true };
        if(firstThreadAccess) {
            std::cout << id << ": first access" << std::endl;
            if( m_pushThreadSlotOccupied.test_and_set(std::memory_order_relaxed)) {
                std::cout << id  << ": too many threads try to access this function" << std::endl;
            } else {
                firstThreadAccess = false;
            }
        }
    }
    void pop(std::string id) {
        thread_local bool firstThreadAccess { true };
        if(firstThreadAccess) {
            std::cout << id << ": first access" << std::endl;
            if( m_popThreadSlotOccupied.test_and_set(std::memory_order_relaxed)) {
                std::cout << id  << ": too many threads try to access this function" << std::endl;
            } else {
                firstThreadAccess = false;
            }
        }
    }
    void resetThreadOwnership() {
            m_pushThreadSlotOccupied.clear(std::memory_order_relaxed);
            m_popThreadSlotOccupied.clear(std::memory_order_relaxed);
    }
};

TEST_CASE("ThreadLocal") {
    ThreadLocalTest c1;
    ThreadLocalTest c2;
    
    auto th1 = std::thread([&] {
        c1.pop("c1 thread 1 - pop");
        c1.pop("c1 thread 1 - pop");
    });
    auto th2 = std::thread([&] {
        c1.push("c1 thread 2 - push");
        c1.push("c1 thread 2 - push");
    });
    auto th3 = std::thread([&] {
        c1.push("c1 thread 3 - push");
        c1.pop("c1 thread 3 - pop");
        c2.push("c2 thread 3 - push");
        c2.pop("c2 thread 3 - pop");
    });
    
    th1.join();
    th2.join();
    th3.join();
    
    c1.resetThreadOwnership();
//     c2.resetThreadOwnership();
    
    auto th4 = std::thread([&] {
        c1.push("c1 thread 4 - push");
        c1.pop("c1 thread 4 - pop");
        c2.push("c2 thread 4 - push");
        c2.pop("c2 thread 4 - pop");
    });
    
    th4.join();
}
