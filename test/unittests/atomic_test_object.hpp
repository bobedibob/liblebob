/*
 * dummy class to test the idea of replacing std classes by mocking them
 * in a non-global std namespace
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_ATOMIC_TEST_OBJECT_HPP_
#define _BOB_ATOMIC_TEST_OBJECT_HPP_

#include <atomic>
#include <iostream>

// this has to be viewed in combination with atomic_test.cpp

class AtomicFoo {
public:
    AtomicFoo() { std::cout << "unsig std::cout in AtomicFoo" << std::endl; }
    
    int push(int data) {
        return m_data.exchange(data, std::memory_order_acq_rel);
    }
    
    int pop() {
        return m_data.exchange(0, std::memory_order_acq_rel);
    }
    
    int peak() {
        return m_data.load(std::memory_order_relaxed);
    }
    
private:
    std::atomic<int> m_data { 73 };
};

#endif // _BOB_ATOMIC_TEST_OBJECT_HPP_
