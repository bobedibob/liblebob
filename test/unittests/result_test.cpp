/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "result/result.hpp"

#include "numbers/random.hpp"

#include "test/mock/bob/assert/asserthandlermock.hpp"

#include "catch.hpp"

#include <utility>

class Number {
public:
    Number() = default;
    constexpr Number(int value) : value(value) { }
    constexpr Number(Number&& rhs) : value(rhs.value) { }
    Number& operator=(Number&& rhs) { if(this != &rhs) {
        this->value = rhs.value; }
        return *this;
    }
    
    Number(const Number&) = delete;
    Number& operator=(const Number& rhs) {
        this->value = rhs.value;
        return *this;
    }
    
    Number operator+(Number& num) {
        return Number(this->value + num.value);
    }
    Number operator/(Number& num) {
        return Number(this->value / num.value);
    }
    
    operator int() const {
        return this->value;
    }
    
private:
    int value { 0 };
};

template <typename T>
bob::result::Result<T> divide_ResultReturn (T& a, T& b) {
    using namespace bob::result;
    
    if(b == 0) { return Err<T>(); }
    return Ok<T>(a/b);
}

template <typename T>
T divide_traditionalReturn (T& a, T& b) {
    if(b == 0) { return T(); }
    return a / b;
}

TEST_CASE("Result - Create a valid result with no type") {
    using namespace bob::result;
    
    auto res = Ok<>();
    REQUIRE(res(Result<>::ErrorHandling) == Res::Ok);
}

TEST_CASE("Result - Create an error result with no type") {
    using namespace bob::result;
    
    auto res = Err<>();
    REQUIRE(res(Result<>::ErrorHandling) == Res::Err);
}

TEST_CASE("Result - Create a valid result with concrete type") {
    using namespace bob::result;
    
    constexpr int expected {42};
    auto res = Ok<int>(static_cast<int>(expected));
    auto value = res(Result<int>::ErrorHandling);
    REQUIRE(value == Res::Ok);
    CHECK(value() == expected);
}

TEST_CASE("Result - Unsafe result access") {
    using namespace bob::result;
    using bob::test::mock::bob::assert::AssertHandlerMock;
    
    auto temporaryAssertHandler = AssertHandlerMock([] (bool condition, const std::string&) {
        if(!condition) {
            throw(true);
        }
    });
    
    auto res = Err<int>();
    REQUIRE_THROWS(res(Result<int>::Unsafe));
}

TEST_CASE("Result - Unsafe access of a result previously accessed by error handling") {
    using namespace bob::result;
    using bob::test::mock::bob::assert::AssertHandlerMock;
    using Result = Result<int>;
    
    int a = 42;
    int b = 13;
    auto expected { divide_traditionalReturn (a, b) };
    
    auto res = divide_ResultReturn(a, b);
    switch(auto data = res(Result::ErrorHandling)) {
        case Res::Ok: CHECK(data() == expected); break;
        case Res::Err: CHECK(false); break;
    }
    
    auto temporaryAssertHandler = AssertHandlerMock([] (bool condition, const std::string&) {
        if(!condition) {
            throw(true);
        }
    });
    
    REQUIRE_THROWS(res(Result::Unsafe));
}

template <typename T>
void benchmark() {
    using namespace bob::result;
    using namespace bob::numbers;
    using Result = Result<T>;

    T a { RNG::fromRange(42, 73) };
    T b { RNG::fromRange(13, 37) };
    auto expected { divide_traditionalReturn (a, b) };
    constexpr T InvalidResult { -1 };
    T result;
    
    BENCHMARK("Traditionla return type") {
        result = divide_traditionalReturn (a, b);
    }
    CHECK(result == expected);
    
    BENCHMARK("Result return type - unsafe access") {
        Result res = divide_ResultReturn (a, b);
        result = res(Result::Unsafe);
    }
    CHECK(result == expected);
    
    BENCHMARK("Result return type - safe access") {
        Result res = divide_ResultReturn (a, b);
        switch(auto value = res(Result::ErrorHandling)) {
            case Res::Ok: result = value(); break;
            case Res::Err: result = InvalidResult; break;
        }
    }
    CHECK(result == expected);
}

TEST_CASE("Result - Benchmark function call with Result return type", "[!benchmark]") {
    using namespace bob::result;
    using namespace bob::numbers;
    
    SECTION("Benchmark with »int« as data type") {
        benchmark<int>();
    }
    
    SECTION("Benchmark with »Number« as data type") {
        benchmark<Number>();
    }
}
