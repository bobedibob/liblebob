/*
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "posix/memcopy.hpp"

#include "test/mock/bob/assert/asserthandlermock.hpp"

#include "catch.hpp"

#include <utility>

TEST_CASE("MemCopy - Perform a successfull memcopy") {
    using bob::posix::MemCopy;
    using bob::result::Res;
    
    int a = 42;
    int b = 0;
    
    REQUIRE(MemCopy<int>().from(a).to(b).copy() == Res::Ok);
    CHECK(a == b);
}

SCENARIO("MemCopy - Check invalid use") {
    using bob::posix::MemCopy;
    using bob::result::Res;
    using bob::test::mock::bob::assert::AssertHandlerMock;
    
    auto temporaryAssertHandler = AssertHandlerMock([] (bool condition, const std::string&) {
        if(!condition) {
            throw(true);
        }
    });
    
    constexpr int ExpectedValue { 42 };
    constexpr int InvalidValue { -1 };
    const int source { ExpectedValue };
    int destination { InvalidValue };
    
    GIVEN("A MemCopy object") {
        bob::posix::MemCopy<int> memCopy;
        
        WHEN("the object was just created") {
            THEN("a copy throws an exception") {
                REQUIRE_THROWS(memCopy.copy());
            }
            THEN("accessing the copy result throws an exception") {
                REQUIRE_THROWS(static_cast<Res>(memCopy));
            }
            
            AND_WHEN("source is set") {
                memCopy.from(source);
                THEN("a copy throws an exception") {
                    REQUIRE_THROWS(memCopy.copy());
                }
                THEN("accessing the copy result throws an exception") {
                    REQUIRE_THROWS(static_cast<Res>(memCopy));
                }
                THEN("setting the source a second time throws an exception") {
                    REQUIRE_THROWS(memCopy.from(source));
                }
                
                AND_WHEN("destination is also set") {
                    memCopy.to(destination);
                    THEN("a copy is successful") {
                        destination = InvalidValue;
                        memCopy.copy();
                        REQUIRE(memCopy == Res::Ok);
                        CHECK(destination == ExpectedValue );
                    }
                }
            }
            
            AND_WHEN("destination is set") {
                memCopy.to(destination);
                THEN("a copy throws an exception") {
                    REQUIRE_THROWS(memCopy.copy());
                }
                THEN("accessing the copy result throws an exception") {
                    REQUIRE_THROWS(static_cast<Res>(memCopy));
                }
                THEN("setting the destination a second time throws an exception") {
                    REQUIRE_THROWS(memCopy.to(destination));
                }
                
                AND_WHEN("source is also set") {
                    memCopy.from(source);
                    THEN("a copy is successful") {
                        destination = InvalidValue;
                        memCopy.copy();
                        REQUIRE(memCopy == Res::Ok);
                        CHECK(destination == ExpectedValue );
                    }
                }
            }
        }
    }
}
