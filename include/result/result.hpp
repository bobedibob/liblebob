/*
 * rust like Result
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_RESULT_TPP_
#define _BOB_RESULT_TPP_

#include "assert/assert.hpp"

#include <ostream>
#include <utility>

/** @file */

namespace bob {
namespace result {

/**
 * @brief Enum to distinguish between error and okay.
 * 
 */
enum class Res {
    Err,    /**< Error enum value.*/
    Ok      /**< Okay enum value.*/
};

//TODO: maybe us an additional error type -> template <typename T, typename E>
/**
 * @brief This class can be used to make error checking for function return values oblicatry.
 * 
 * Example usage:
 * @include doc/snippets/result/result_example.cpp
 */
template <typename T = bool>
class Result {
private:
    struct Unsafe_t {};
    struct ErrorHandling_t {};
    
public:
    
    /**
    * @brief Guard to access the result value from within a switch statement.
    * 
    * @snippet doc/snippets/result/result_example.cpp Getting a result
    */
    class AccessGuard {
        friend class Result;
    public:
        AccessGuard() = delete;
        
        AccessGuard(const AccessGuard&) = delete;
        AccessGuard& operator=(const AccessGuard&) = delete;
        
        AccessGuard(AccessGuard&&) = default;
        AccessGuard& operator=(AccessGuard&&) = delete;
        
        ~AccessGuard() {}
        
        
        T& operator()() {
            return *data;
        }
        
        T* operator-> () {
            return data;
        }
        
        /**
        * @brief Enable implicit type cast to Res enum, needed in the switch statement.
        * 
        * @return bob::result::Result< T >::Type
        */
        operator Res() const { return result.type; }
        
    private:
        AccessGuard(Result<T>&& result) : result(std::move(result)) {
            if(this->result.type == Res::Ok) { data = &this->result.data.value; }
        }
        
        T* data { nullptr };
        Result<T> result;
    };
    
    static constexpr Unsafe_t Unsafe {};                /**< Function call operator parameter for unsafe overload.*/
    static constexpr ErrorHandling_t ErrorHandling {};  /**< Function call operator parameter for error handlings overload.*/
    
    /**
    * @brief copy ctor is deleted
    */
    Result(const Result&) = delete;
    /**
    * @brief copy assignment operator is deleted
    */
    Result& operator=(const Result&) = delete;
    
    /**
    * @brief dtor
    */
    ~Result() {
        if(type == Res::Ok) { data.value.~T(); }
    }
    
    /**
    * @brief move ctor
    * 
    */
    Result(Result&& rhs /**< [in] r-value for move ctor.*/) {
        if(type == Res::Ok) { 
            if(rhs.type == Res::Ok) { data.value = std::move(rhs.data.value); }
            if(rhs.type == Res::Err) { data.value.~T(); data.none = Empty_t(); }
        } else {
            if(rhs.type == Res::Ok) { new (&data.value) T(std::move(rhs.data.value)); }
        }
        type = rhs.type;
        rhs.type = Res::Err;
    }
    
    /**
    * @brief move assignment operator
    * 
    * @return Result< T >&
    */
    Result& operator=(Result&& rhs /**< [in] r-value for move assignment operator.*/) {
        if(this != &rhs) {
            *this = rhs;
        }
        return *this;
    }
    
    /**
    * @brief Function call operator for error handling access. The Result instance is moved to the AccessGuard. Any subsequent Result access is invalid and UB.
    * 
    * @snippet doc/snippets/result/result_example.cpp Getting a result
    * 
    * @return bob::result::Result< T >::AccessGuard
    */
    AccessGuard operator()(ErrorHandling_t /**< [in] Result::ErrorHandling constexpr must be used.*/) {
        return AccessGuard(std::move(*this));
    }
    
    /**
    * @brief Function call operator for unsafe access.
    * 
    * @return T&
    */
    T& operator()(Unsafe_t /**< [in] Result::Unsafe constexpr must be used*/) {
        assert::Ensures(type == Res::Ok, "Accessing an invalid result!");
        
        return data.value;
    }
    
protected:
    /**
    * @brief empty type for data union type
    */
    struct Empty_t {};
    static constexpr Empty_t Empty {};
    
    /**
    * @brief data union contains the value if Res::Ok or none if Res::Err
    */
    union Data_t {
        Empty_t none;
        T value;
    };
    
    /**
    * @brief ctor for Res::Ok
    */
    Result(T&& value /**< [in] the result value*/) : type(Res::Ok) {
        new (&data.value) T(std::move(value));
    }
    
    /**
    * @brief ctor for Res::Err
    */
    Result() : type(Res::Err) {
    }
    
private:
    Res type { Res::Err };
    Data_t data { Empty_t() };
};

/**
 * @brief Dummy class to be able to have a class with no template arguments
 */
template <typename...>
class Ok {};

/**
 * @brief Create a valid result for a concrete type
 * 
 * @snippet doc/snippets/result/result_example.cpp Using Ok and Err
 */
template <typename T>
class Ok<T> : public Result<T> {
public:
    /**
    * @brief ctor for valie result
    */
    Ok(T&& value /**< [in] the result value*/) : Result<T>(std::move(value)) {}
};

/**
 * @brief Create a valid result for no type arguments
 * 
 * @snippet doc/snippets/result/result_example.cpp Using Ok and Err
 */
template <>
class Ok<> : public Result<bool> {
public:
    /**
    * @brief ctor for valie result
    */
    Ok() : Result<bool>(true) {}
};


/**
 * @brief Dummy class to be able to have a class with no template arguments
 */
template <typename...>
class Err {};

/**
 * @brief Create an error result for a concrete type
 * 
 * @snippet doc/snippets/result/result_example.cpp Using Ok and Err
 */
template <typename T>
class Err<T> : public Result<T> {
public:
    /**
    * @brief ctor for error result
    */
    Err() : Result<T>() {}
};

/**
 * @brief Create an error result for no type arguments
 * 
 * @snippet doc/snippets/result/result_example.cpp Using Ok and Err
 */
template <>
class Err<> : public Result<bool> {
public:
    /**
    * @brief ctor for error result
    */
    Err() : Result<bool>() {}
};

} // namespace result
} // namespace bob


/**
 * @brief Stream operator for Res
 * 
 * @return std::ostream&
 */
inline std::ostream& operator<<(std::ostream& stream /**< [in] the output stream operator.*/,
                                bob::result::Res res /**< [in] the Res enum value.*/) {
    if(res == bob::result::Res::Ok) { stream << "Res::Ok"; }
    else { stream << "Res::Err"; }
    
    return stream;
}

#endif // _BOB_RESULT_TPP_
