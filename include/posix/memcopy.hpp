/*
 * posix memcpy wrapper
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
 
#ifndef _BOB_MEMCOPY_HPP_
#define _BOB_MEMCOPY_HPP_

#include <assert/assert.hpp>
#include "result/result.hpp"

#include <memory.h>

namespace bob {
namespace posix {

template <typename T>
class MemCopy {
public:
    MemCopy() = default;
    ~MemCopy() {
        try {
            assert::Expects(m_executed, "It doesn't make sense to create a MemCopy object without performing the copy!");
        } catch (...) {}
    }
    MemCopy(const MemCopy&) = delete;
    MemCopy(MemCopy&&) = delete;
    
    MemCopy& operator=(const MemCopy&) = delete;
    MemCopy& operator=(MemCopy&&) = delete;
    
    MemCopy& from(const T& source) {
        assert::Expects(m_source == nullptr, "Setting the source multiple times is not supported!");
        
        m_source = &source;
        return *this;
    }
    
    MemCopy& to(T& destination) {
        assert::Expects(m_destination == nullptr, "Setting the destination multiple times is not supported!");
        
        m_destination = &destination;
        return *this;
    }
    
    MemCopy& copy() {
        assert::Expects(m_source != nullptr, "Source must be set before copying!");
        assert::Expects(m_destination != nullptr, "Destination must be set before copying!");
        //TODO: the memory areas to copy must not overlap, so this should be checked;
        //      this shouldn't be a problem with this implemetation, though
        
        memcpy(m_destination, m_source, sizeof(T));
        m_executed = true;
        m_wasSuccessful = true;
        
        return *this;
    }
    
    operator bob::result::Res() const {
        assert::Expects(m_executed, "Copy was not performed! It doesn't make sense to query for the result!");
        
        using bob::result::Res;
        return m_wasSuccessful ? Res::Ok : Res::Err;
    }
    
private:
    const T* m_source { nullptr };
    T* m_destination { nullptr };
    
    bool m_executed { false };
    bool m_wasSuccessful { false };
};

} // namespace posix
} // namespace bob

#endif // _BOB_MEMCOPY_HPP_
