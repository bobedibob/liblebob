/*
 * random number generator
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_RANDOM_H_
#define _BOB_RANDOM_H_

#include <random>

namespace bob {
namespace numbers {
namespace RNG {

/**
* @brief random number generator
* 
* @param T: template type
* @param min: the min value of the range (inclusive)
* @param max: the max value of the range (inclusive)
* @return T: the random number
*/
template <typename T>
T fromRange(T min, T max, typename std::enable_if<std::is_integral<T>::value >::type* = 0) {
    static std::random_device randomDevice;
    std::uniform_int_distribution<T> distribution(min, max);
    return distribution(randomDevice);
}

/**
* @brief random number generator
* 
* @param T: template type
* @param min: the min value of the range (inclusive)
* @param max: the max value of the range (inclusive)
* @return T: the random number
*/
template <typename T>
T fromRange(T min, T max, typename std::enable_if<std::is_floating_point<T>::value >::type* = 0) {
    static std::random_device randomDevice;
    std::uniform_real_distribution<T> distribution(min, max);
    return distribution(randomDevice);
}

} // namespace RNG
} // namespace numbers
} // namespace bob

#endif // _BOB_RANDOM_H_
