/*
 * small helper for colored console output
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_RICHTEXT_HPP_
#define _BOB_RICHTEXT_HPP_

#include <initializer_list>
#include <ostream>

namespace bob {
namespace console {

// see https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
/**
 * @brief Color codes
 * 
 * see https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 * 
 */
struct Color {
    enum {
        Black   = 30,
        Red     = 31,
        Green   = 32,
        Yellow  = 33,
        Blue    = 34,
        Magenta = 35,
        Cyan    = 36,
        White   = 37,
        Default = 39
    };
    struct Bg {
        enum {
            Black   = 40,
            Red     = 41,
            Green   = 42,
            Yellow  = 43,
            Blue    = 44,
            Magenta = 45,
            Cyan    = 46,
            White   = 47,
            Default = 49
        };
    };
};

/**
 * @brief Style codes
 * 
 * see https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
 * 
 */
struct Style {
    enum {
        Bright      = 1,
        Underline   = 4,
        Blink       = 5,
        Inverse     = 7,
        Crossedout  = 9
    };
    struct Off {
        enum {
            Bright      = 21,
            Underline   = 24,
            Blink       = 25,
            Inverse     = 27,
            Crossedout  = 29
        };
    };
};

/**
 * @brief Formatter for the console.
 * 
 * @code{.cpp}
 * using namespace bob::console;
 * std::cout << RichText{{Color::Red, Style::Blink}} << "Timeout" << RichText{{RichText::Reset}} << std::endl;
 * @endcode
 * 
 */
struct RichText {
    const std::initializer_list<int> codes;
    static constexpr int Reset { 0 };
};

inline std::ostream& operator<<(std::ostream& stream, const RichText format) {
    stream << "\033[";
    bool first { true };
    for(const auto& code: format.codes) {
        if(!first) { stream << ";"; }
        stream << code;
        first = false;
    }
    stream << "m";
    return stream;
}

} // namespace console
} // namespace bob

#endif // _BOB_RICHTEXT_HPP_
