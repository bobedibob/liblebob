/*
 * asserts options for release, debug and test builds
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_ASSERT_HPP_
#define _BOB_ASSERT_HPP_

#include <functional>
#include <string>

namespace bob {
namespace assert {

void Expects(const bool condition);
void Expects(const bool condition, const std::string& description);

void Ensures(const bool condition);
void Ensures(const bool condition, const std::string& description);

class AssertHandler {
public:
    static void doAssert(const bool condition, const std::string& description);

protected:
    static std::function<void(const bool, const std::string&)> handler;
    static void defaultHandler(const bool condition, const std::string& description);
    static void noRuntimeChecksHandler(const bool condition, const std::string& description);
};

} // namespace assert
} // namespace bob

#endif // _BOB_ASSERT_HPP_
