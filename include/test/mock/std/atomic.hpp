/*
 * mock for std::atomic exchange fuction
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _BOB_ATOMIC_HPP_
#define _BOB_ATOMIC_HPP_

#include <atomic>
#include <iostream>

namespace bob {
namespace test {
namespace mock {
namespace std {

template <typename T>
class atomic : public ::std::atomic<T> {
public:
    atomic() {
        ::std::cout << "my mock atomic" << ::std::endl;
    }
    constexpr atomic(T value) noexcept : ::std::atomic<T>(value) { }
    
    T exchange(T value, ::std::memory_order mem_order) {
        if(atomic<T>::s_overrideExchangeValue) { value = atomic<T>::s_exchangeValue; }
        T oldValue = ::std::atomic<T>::exchange(value, mem_order);
        ::std::cout << "exchanged " << oldValue << " with " << value << ::std::endl;
        return oldValue;
    }
    
    static void setOverrideExchangeValue(T value) {
        s_exchangeValue = value;
        s_overrideExchangeValue = true;
    }
    
    static void unsetOverrideExchangeValue() {
        s_overrideExchangeValue = false;
    }
    
private:
    static T s_exchangeValue;
    static bool s_overrideExchangeValue;
};

template <typename T>
T atomic<T>::s_exchangeValue;
template <typename T>
bool atomic<T>::s_overrideExchangeValue { false };

} // namespace std
} // namespace mock
} // namespace test
} // namespace bob

#endif // _BOB_ATOMIC_HPP_
