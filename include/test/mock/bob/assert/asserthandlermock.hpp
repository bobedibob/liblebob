/*
 * asserts options for release, debug and test builds
 * Copyright (C) 2018  Mathias Kraus <elboberido@m-hias.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef _ASSERTHANDLERMOCK_HPP_
#define _ASSERTHANDLERMOCK_HPP_

#include "assert/assert.hpp"

namespace bob {
namespace test {
namespace mock {
namespace bob {
namespace assert {

class AssertHandlerMock : public ::bob::assert::AssertHandler {
public:
    AssertHandlerMock(std::function<void(const bool, const std::string&)> handler) {
        AssertHandler::handler = handler;
    }
    ~AssertHandlerMock() { if(!moved) { AssertHandler::handler = AssertHandler::defaultHandler; } }

    AssertHandlerMock(AssertHandlerMock&& other) { other.moved = true; }
    AssertHandlerMock& operator=(AssertHandlerMock&&) = delete;

    AssertHandlerMock(const AssertHandlerMock&) = delete;
    AssertHandlerMock& operator=(const AssertHandlerMock&) = delete;
private:
    bool moved { false };
};

} // namespace assert
} // namespace bob
} // namespace mock
} // namespace test
} // namespace bob

#endif // _ASSERTHANDLERMOCK_HPP_
